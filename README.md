# Employees Demo App (JAVA API) @

This Demo App shows how to design and implement REST API in Java using Spring + Jersey (JAX-RS)

## Prerequisites:
* Git
* JDK 1.8
* Maven 3.3.9
* MongoDB 3.2.9

## Install and run the project 

This instruction works for clean machine with Linux OS installed

### Download and Install JDK

```
[~] # cd /opt/
[opt] # wget --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2F; oraclelicense=accept-securebackup-cookie" "http://download.oracle.com/otn-pub/java/jdk/8u102-b14/jdk-8u102-linux-x64.tar.gz"
[opt] # tar xzf jdk-8u102-linux-x64.tar.gz
[opt] # cd jdk1.8.0_102
[jdk1.8.0_102] # alternatives --install /usr/bin/java java /opt/jdk1.8.0_102/bin/java 2
[jdk1.8.0_102] # alternatives --config java
[jdk1.8.0_102] # alternatives --install /usr/bin/jar jar /opt/jdk1.8.0_102/bin/jar 2
[jdk1.8.0_102] # alternatives --install /usr/bin/javac javac /opt/jdk1.8.0_102/bin/javac 2
[jdk1.8.0_102] # alternatives --set jar /opt/jdk1.8.0_102/bin/jar
[jdk1.8.0_102] # alternatives --set javac /opt/jdk1.8.0_102/bin/javac
[jdk1.8.0_102] # export JAVA_HOME=/opt/jdk1.8.0_102
[jdk1.8.0_102] # export JRE_HOME=/opt/jdk1.8.0_102/jre
[jdk1.8.0_102] # export PATH=#PATH:/opt/jdk1.8.0_101/bin:/opt/jdk1.8.0_102/jre/bin
```

### Download and Install Maven

```
[opt] # wget http://mirror.wanxp.id/apache/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
[opt] # tar xzf apache-maven-3.3.9-bin.tar.gz
[opt] # export PATH=$PATH:/opt/apache-maven-3.3.9/bin
[opt] # export MAVEN_OPTS=-Xmx256m
```

### Download and Install Git

```
[~] # apt-get install git
```

### Clone the project

```
[~] # git clone git@bitbucket.org:istratov_petr/tallink-employees-api.git
```

### Download and Install MongoDB

```
[opt] # curl -O https://fastdl.mongodb.org/linux/mongodb-linux-x86_64-3.2.9.tgz
[opt] # tar -zxvf mongodb-linux-x86_64-3.2.9.tgz
[opt] # export PATH=/opt/mongodb-linux-x86_64-3.2.9/bin:$PATH
```

### Set up MongoDB

```
[~] # mkdir mongodb
[~] # mongod --dbpath ~/mongodb
[~] # mongo

> use tallink
> db.createUser({user: <username>,pwd: <password>,roles: [ { role: "userAdmin", db: "tallink" } ]});
>  db.createUser({user: <username>, pwd: <password>,  roles: [ { role: "readWrite", db: "tallink" },{ role: "read", db: "tallink" } ]});
> load('tallink-employees-api/migration/demoDbDump.js')

close mongo via CTRL+C
close mongod via CTRL+C

[~] # mongod --auth --dbpath ~/mongodb
```

### Set up project config

```
[~] # cd tallink-employees-api/src/main/resources/config
```

Open `db.properties` and set your db credentials as follows:

```
mongodb.user=<username>
mongodb.pwd=<password>
```

### Start Application

```
[~] # cd tallink-employees-api
[tallink-employees-api] # mvn clean install jetty:run -Djetty.port=8888 -DskipTests=true
```

Now the REST api is up and running with Jetty on `localhost:8888` 
  
> **Note:** you could run a similar configuration from Eclipse if you have the m2e plugin installed

> **Note:** after you `mvn install` the application, you can deploy the generated __.war__ file in any web container like Tomcat for example. 

> **Note:** you can see example of frontend Meteor Application using this API here https://bitbucket.org/istratov_petr/tallink-employees-meteor

## Architecture scheme

![Architecture scheme](http://smages.com/images/tallinkemp.png)
