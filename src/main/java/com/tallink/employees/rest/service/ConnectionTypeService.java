package com.tallink.employees.rest.service;

import com.mongodb.WriteResult;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.resource.connection_type.ConnectionType;
import org.bson.Document;

import java.util.List;

public interface ConnectionTypeService {

    public List<ConnectionType> getConnectionTypes(Document filters) throws AppException;

    public List<WriteResult> upsertConnectionTypes(List<ConnectionType> connectionTypes) throws AppException;

    public List<WriteResult> deleteConnectionTypes(List<String> ids) throws AppException;

}
