package com.tallink.employees.rest.service;

import com.mongodb.WriteResult;
import com.tallink.employees.rest.errorhandling.AppException;
import org.bson.Document;
import com.tallink.employees.rest.dao.ConnectionTypeDao;
import com.tallink.employees.rest.dao.ConnectionTypeEntity;
import com.tallink.employees.rest.resource.connection_type.ConnectionType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class ConnectionTypeServiceDbAccessImpl implements ConnectionTypeService {

    @Autowired
    ConnectionTypeDao connectionTypeDao;

    public List<ConnectionType> getConnectionTypes(Document filters) throws AppException {

        List<ConnectionTypeEntity> connectionTypes = connectionTypeDao.getConnectionTypes(filters);

        return getConnectionTypesFromEntities(connectionTypes);
    }

    private List<ConnectionType> getConnectionTypesFromEntities(List<ConnectionTypeEntity> connectionTypeEntities) {
        List<ConnectionType> response = new ArrayList<>();
        for(ConnectionTypeEntity connectionTypeEntity : connectionTypeEntities){
            response.add(new ConnectionType(connectionTypeEntity));
        }

        return response;
    }

    @Transactional("transactionManager")
    public List<WriteResult> upsertConnectionTypes(List<ConnectionType> connectionTypes) throws AppException {

        return connectionTypeDao.upsertConnectionTypes(getEntitiesFromConnectionTypes(connectionTypes));
    }

    private List<ConnectionTypeEntity> getEntitiesFromConnectionTypes(List<ConnectionType> connectionTypes) {
        List<ConnectionTypeEntity> mappedConnectionTypes = new ArrayList<>();

        for(ConnectionType connectionType : connectionTypes){
            mappedConnectionTypes.add(new ConnectionTypeEntity(connectionType));
        }

        return mappedConnectionTypes;
    }

    @Transactional("transactionManager")
    public List<WriteResult> deleteConnectionTypes(List<String> ids) throws AppException {
        return connectionTypeDao.deleteConnectionTypes(ids);
    }
}
