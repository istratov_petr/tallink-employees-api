package com.tallink.employees.rest.service;

import com.mongodb.WriteResult;
import org.bson.Document;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.resource.connection.Connection;

import java.util.List;

public interface ConnectionService {

    public List<Connection> getConnections(Document filters) throws AppException;

    public List<WriteResult> insertConnections(List<Connection> connections) throws AppException;

    public List<WriteResult> deleteConnections(List<String> ids) throws AppException;

}
