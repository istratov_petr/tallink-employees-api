package com.tallink.employees.rest.service;

import com.mongodb.WriteResult;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.resource.employee.Employee;
import org.bson.Document;

import java.util.List;

public interface EmployeeService {

    public List<Employee> getEmployees(Document filters) throws AppException;

    public List<WriteResult> upsertEmployees(List<Employee> employees) throws AppException;

    public List<WriteResult> deleteEmployees(List<String> ids) throws AppException;

}
