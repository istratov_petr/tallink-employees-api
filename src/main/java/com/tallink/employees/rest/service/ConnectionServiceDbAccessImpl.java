package com.tallink.employees.rest.service;

import com.mongodb.WriteResult;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.resource.connection.Connection;
import org.bson.Document;
import com.tallink.employees.rest.dao.ConnectionDao;
import com.tallink.employees.rest.dao.ConnectionEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public class ConnectionServiceDbAccessImpl implements ConnectionService {

    @Autowired
    ConnectionDao connectionDao;

    public List<Connection> getConnections(Document filters) throws AppException {

        List<ConnectionEntity> connections = connectionDao.getConnections(filters);

        return getConnectionsFromEntities(connections);
    }

    private List<Connection> getConnectionsFromEntities(List<ConnectionEntity> connectionEntities) {
        List<Connection> response = new ArrayList<>();
        for(ConnectionEntity connectionEntity : connectionEntities){
            response.add(new Connection(connectionEntity));
        }

        return response;
    }

    @Transactional("transactionManager")
    public List<WriteResult> insertConnections(List<Connection> connections) throws AppException {

        return connectionDao.insertConnections(getEntitiesFromConnections(connections));
    }

    private List<ConnectionEntity> getEntitiesFromConnections(List<Connection> connections) {
        List<ConnectionEntity> mappedConnections = new ArrayList<>();

        for(Connection connection : connections){
            mappedConnections.add(new ConnectionEntity(connection));
        }

        return mappedConnections;
    }

    @Transactional("transactionManager")
    public List<WriteResult> deleteConnections(List<String> ids) throws AppException {
        return connectionDao.deleteConnections(ids);
    }

}
