package com.tallink.employees.rest.service;

import com.mongodb.WriteResult;
import com.sun.research.ws.wadl.Doc;
import com.tallink.employees.rest.dao.*;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.resource.connection.Connection;
import com.tallink.employees.rest.resource.employee.Employee;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Arrays;
import com.google.gson.Gson;

public class EmployeeServiceDbAccessImpl implements EmployeeService {

    @Autowired
    EmployeeDao employeeDao;

    @Autowired
    ConnectionDao connectionDao;

    public List<Employee> getEmployees(Document filters) throws AppException {

        List<EmployeeEntity> employees = employeeDao.getEmployees(filters);

        return getEmployeesFromEntities(employees);
    }

    private List<Employee> getEmployeesFromEntities(List<EmployeeEntity> employeeEntities) {
        List<Employee> response = new ArrayList<>();
        for(EmployeeEntity employeeEntity : employeeEntities){
            response.add(new Employee(employeeEntity));
        }

        return response;
    }

    @Transactional("transactionManager")
    public List<WriteResult> upsertEmployees(List<Employee> employees) throws AppException {

        return employeeDao.upsertEmployees(getEntitiesFromEmployees(employees));
    }

    private List<EmployeeEntity> getEntitiesFromEmployees(List<Employee> employees) {
        List<EmployeeEntity> mappedEmployees = new ArrayList<>();

        for(Employee employee : employees){
            mappedEmployees.add(new EmployeeEntity(employee));
        }

        return mappedEmployees;
    }

    @Transactional("transactionManager")
    public List<WriteResult> deleteEmployees(List<String> ids) throws AppException {

        //check if there are broken connections
        final List<ConnectionEntity> newConnections = new ArrayList<>();

        final Document filters = new Document("$or", Arrays.asList(
                new Document("to", new Document("$in", ids)),
                new Document("from", new Document("$in", ids))
        ));

        final List<ConnectionEntity> oldConnections = connectionDao.getConnections(filters);
        List<String> connectionsToDelete = new ArrayList<>();

        for (String id : ids) {
            List<ConnectionEntity> parents = new ArrayList<>();
            List<ConnectionEntity> children = new ArrayList<>();

            for (ConnectionEntity item : oldConnections) {
                if(item.getTo().equals(id)) parents.add(item);
                if(item.getFrom().equals(id)) children.add(item);
                connectionsToDelete.add(item.getId());
            }

            for (ConnectionEntity parent : parents) {
                for (ConnectionEntity child : filterConnectionsByType(parent.getConnection_types_id(), children)) {
                    ConnectionEntity newConnection = new ConnectionEntity();
                    newConnection.setId(new String().valueOf(Math.random() * 1e7));
                    newConnection.setConnection_types_id(parent.getConnection_types_id());
                    newConnection.setFrom(parent.getFrom());
                    newConnection.setTo(child.getTo());
                    newConnection.setCreatedAt(new Date());
                    newConnection.setOwner(parent.getOwner());
                    newConnection.setUserName(parent.getUserName());
                    newConnection.setPrivateSwitch(parent.getPrivateSwitch());

                    newConnections.add(newConnection);
                }
            }
        }

        connectionDao.deleteConnections(connectionsToDelete);
        connectionDao.insertConnections(newConnections);

        return employeeDao.deleteEmployees(ids);
    }

    private List<ConnectionEntity> filterConnectionsByType(String type, List<ConnectionEntity> connections) {
        List<ConnectionEntity> result = new ArrayList<>();
        for (ConnectionEntity connection : connections) {
            if(connection.getConnection_types_id().equals(type)) result.add(connection);
        }
        return result;
    }

}
