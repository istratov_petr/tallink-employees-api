package com.tallink.employees.rest;

import org.glassfish.jersey.message.GZipEncoder;
import org.glassfish.jersey.message.filtering.EntityFilteringFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.EncodingFilter;

public class RestDemoJaxRsApplication extends ResourceConfig {

	public RestDemoJaxRsApplication() {
		
        packages("com.tallink.employees.rest");

		register(EntityFilteringFeature.class);
		EncodingFilter.enableFor(this, GZipEncoder.class);		
		
	}
}
