package com.tallink.employees.rest.util;

import com.google.gson.Gson;

import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import com.tallink.employees.rest.helpers.GsonTypeAdapter;


public class AppUtils {

    public static DBObject toDBObject(Object pojo) {

        Gson gson = GsonTypeAdapter.getGsonBuilder(GsonTypeAdapter.GsonAdapterType.SERIALIZER).create();
        String json = gson.toJson(pojo);

        return (DBObject) JSON.parse(json);

    }

    public static Object fromDBObject(DBObject dbObj, Class clazz) {

        Gson gson = GsonTypeAdapter.getGsonBuilder(GsonTypeAdapter.GsonAdapterType.DESERIALIZER).create();

        return gson.fromJson(dbObj.toString(), clazz);
    }

}