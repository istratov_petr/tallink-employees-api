package com.tallink.employees.rest.resource.employee;

import com.tallink.employees.rest.dao.EmployeeEntity;
import com.tallink.employees.rest.helpers.DateISO8601Adapter;
import org.apache.commons.beanutils.BeanUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee implements Serializable {

    private static final long serialVersionUID = -8039686696076337053L;

    @XmlElement(name = "_id")
    private String _id;

    @XmlElement(name="name")
    private String name;

    @XmlElement(name="position")
    private String position;

    @XmlElement(name="createdAt")
    @XmlJavaTypeAdapter(DateISO8601Adapter.class)
    private Date createdAt;

    @XmlElement(name="owner")
    private String owner;

    @XmlElement(name="userName")
    private String userName;

    @XmlElement(name="privateSwitch")
    private Boolean privateSwitch;

    public Employee(EmployeeEntity employeeEntity){
        try {
            BeanUtils.copyProperties(this, employeeEntity);
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Employee(String _id, String name, String position, Date createdAt, String owner,
                    String userName, Boolean privateSwitch) {

        this._id = _id;
        this.name = name;
        this.position = position;
        this.createdAt = createdAt;
        this.owner = owner;
        this.userName = userName;
        this.privateSwitch = privateSwitch;

    }

    public Employee(){}

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) { this.position = position; }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) { this.owner = owner; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) { this.userName = userName; }

    public Boolean getPrivateSwitch() {
        return privateSwitch;
    }

    public void setPrivateSwitch(Boolean privateSwitch) { this.privateSwitch = privateSwitch; }

}
