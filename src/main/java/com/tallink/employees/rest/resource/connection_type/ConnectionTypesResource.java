package com.tallink.employees.rest.resource.connection_type;

import com.mongodb.WriteResult;
import org.bson.Document;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.service.ConnectionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Component
@Path("/connection_types")
public class ConnectionTypesResource {

    @Autowired
    private ConnectionTypeService connectionTypeService;

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public List<ConnectionType> getConnectionTypes(Document filters)
            throws IOException, AppException {
        List<ConnectionType> connectionTypes = connectionTypeService.getConnectionTypes(filters);
        return connectionTypes;
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response upsertConnectionTypes(List<ConnectionType> connectionTypes) throws AppException {

        List connectionTypeIds = connectionTypeService.upsertConnectionTypes(connectionTypes);
        GenericEntity<List<WriteResult>> entity = new GenericEntity<List<WriteResult>>(connectionTypeIds) {};
        return Response
                .status(Response.Status.OK)
                // 200
                .entity(entity)
                .build();
    }

    @DELETE
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response deleteConnectionTypes(List<String> ids) throws AppException {

        List connectionTypeIds = connectionTypeService.deleteConnectionTypes(ids);
        GenericEntity<List<WriteResult>> entity = new GenericEntity<List<WriteResult>>(connectionTypeIds) {};
        return Response
                .status(Response.Status.OK)
                // 200
                .entity(entity)
                .build();
    }

    public void setConnectionTypeService(ConnectionTypeService connectionTypeService) {
        this.connectionTypeService = connectionTypeService;
    }

}