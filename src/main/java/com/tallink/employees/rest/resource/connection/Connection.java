package com.tallink.employees.rest.resource.connection;

import com.tallink.employees.rest.helpers.DateISO8601Adapter;
import org.apache.commons.beanutils.BeanUtils;
import com.tallink.employees.rest.dao.ConnectionEntity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@SuppressWarnings("restriction")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Connection implements Serializable {

    private static final long serialVersionUID = -8039686696076337053L;

    @XmlElement(name = "_id")
    private String _id;

    @XmlElement(name="connection_types_id")
    private String connection_types_id;

    @XmlElement(name="from")
    private String from;

    @XmlElement(name="to")
    private String to;

    @XmlElement(name="createdAt")
    @XmlJavaTypeAdapter(DateISO8601Adapter.class)
    private Date createdAt;

    @XmlElement(name="owner")
    private String owner;

    @XmlElement(name="userName")
    private String userName;

    @XmlElement(name="privateSwitch")
    private Boolean privateSwitch;

    public Connection(ConnectionEntity connectionEntity){
        try {
            BeanUtils.copyProperties(this, connectionEntity);
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Connection(String _id, String connection_types_id, String from, String to, Date createdAt, String owner,
                    String userName, Boolean privateSwitch) {

        this._id = _id;
        this.connection_types_id = connection_types_id;
        this.from = from;
        this.to = to;
        this.createdAt = createdAt;
        this.owner = owner;
        this.userName = userName;
        this.privateSwitch = privateSwitch;

    }

    public Connection(){}

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getConnection_types_id() {
        return connection_types_id;
    }

    public void setConnection_types_id(String connection_types_id) { this.connection_types_id = connection_types_id; }

    public String getTo() {
        return to;
    }

    public void setTo(String to) { this.to = to; }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) { this.from = from; }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) { this.owner = owner; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) { this.userName = userName; }

    public Boolean getPrivateSwitch() {
        return privateSwitch;
    }

    public void setPrivateSwitch(Boolean privateSwitch) { this.privateSwitch = privateSwitch; }
}
