package com.tallink.employees.rest.resource.connection;

import com.mongodb.WriteResult;
import com.tallink.employees.rest.errorhandling.AppException;
import com.tallink.employees.rest.service.ConnectionService;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Component
@Path("/connections")
public class ConnectionsResource {

    @Autowired
    private ConnectionService connectionService;

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public List<Connection> getConnections(Document filters)
            throws IOException, AppException {
        List<Connection> connections = connectionService.getConnections(filters);
        return connections;
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response insertConnections(List<Connection> connections) throws AppException {

        List connectionIds = connectionService.insertConnections(connections);
        GenericEntity<List<WriteResult>> entity = new GenericEntity<List<WriteResult>>(connectionIds) {};
        return Response
                .status(Response.Status.OK)
                // 200
                .entity(entity)
                .build();
    }

    @DELETE
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response deleteConnections(List<String> ids) throws AppException {

        List connectionIds = connectionService.deleteConnections(ids);
        GenericEntity<List<WriteResult>> entity = new GenericEntity<List<WriteResult>>(connectionIds) {};
        return Response
                .status(Response.Status.OK)
                // 200
                .entity(entity)
                .build();
    }

    public void setConnectionService(ConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
