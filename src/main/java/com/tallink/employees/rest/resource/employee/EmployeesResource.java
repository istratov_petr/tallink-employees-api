package com.tallink.employees.rest.resource.employee;

import com.mongodb.WriteResult;
import com.tallink.employees.rest.errorhandling.AppException;
import org.bson.Document;
import com.tallink.employees.rest.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;

@Component
@Path("/employees")
public class EmployeesResource {

    @Autowired
    private EmployeeService employeeService;

    @GET
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public List<Employee> getEmployees(Document filters)
            throws IOException, AppException {
        List<Employee> employees = employeeService.getEmployees(filters);
        return employees;
    }

    @POST
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response upsertEmployees(List<Employee> employees) throws AppException {

        List employeeIds = employeeService.upsertEmployees(employees);
        GenericEntity<List<WriteResult>> entity = new GenericEntity<List<WriteResult>>(employeeIds) {};
        return Response
                .status(Response.Status.OK)
                // 200
                .entity(entity)
                .build();
    }

    @DELETE
    @Consumes({ MediaType.APPLICATION_JSON })
    @Produces({ MediaType.APPLICATION_JSON })
    public Response deleteEmployees(List<String> ids) throws AppException {

        List employeeIds = employeeService.deleteEmployees(ids);
        GenericEntity<List<WriteResult>> entity = new GenericEntity<List<WriteResult>>(employeeIds) {};
        return Response
                .status(Response.Status.OK)
                // 200
                .entity(entity)
                .build();
    }

    public void setEmployeeService(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

}
