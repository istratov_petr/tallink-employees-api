package com.tallink.employees.rest.dao;

import org.apache.commons.beanutils.BeanUtils;
import com.tallink.employees.rest.resource.employee.Employee;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@Entity
@Table(name="employees")
public class EmployeeEntity implements Serializable {

    private static final long serialVersionUID = -8039686696076337053L;

    @Id
    @GeneratedValue
    @Column(name="_id")
    private String _id;

    @Column(name="name")
    private String name;

    @Column(name="position")
    private String position;

    @Column(name="createdAt")
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @Column(name="owner")
    private String owner;

    @Column(name="userName")
    private String userName;

    @Column(name="privateSwitch")
    private Boolean privateSwitch;

    public EmployeeEntity(){}

    public EmployeeEntity(String _id, String name, String position, String owner,
                         String userName, Boolean privateSwitch) {

        this._id = _id;
        this.name = name;
        this.position = position;
        this.owner = owner;
        this.userName = userName;
        this.privateSwitch = privateSwitch;

    }

    public EmployeeEntity(Employee employee){
        try {
            BeanUtils.copyProperties(this, employee);
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) { this.name = name; }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) { this.position = position; }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) { this.owner = owner; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) { this.userName = userName; }

    public Boolean getPrivateSwitch() {
        return privateSwitch;
    }

    public void setPrivateSwitch(Boolean privateSwitch) { this.privateSwitch = privateSwitch; }

}
