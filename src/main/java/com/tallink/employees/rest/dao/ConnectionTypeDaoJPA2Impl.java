package com.tallink.employees.rest.dao;

import com.mongodb.*;
import com.tallink.employees.rest.util.AppUtils;
import org.bson.Document;

import javax.annotation.Nonnull;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConnectionTypeDaoJPA2Impl implements ConnectionTypeDao {

    private MongoClient mongoClient;
    private String dbName;
    private String collectionName;
    private DBCollection connectionTypesCollection;

    public void init() throws UnknownHostException {
        DB tallinkDatabase = mongoClient.getDB(dbName);
        connectionTypesCollection = tallinkDatabase.getCollection(collectionName);
    }

    public List<ConnectionTypeEntity> getConnectionTypes(Document filters) {

        final List<ConnectionTypeEntity> connectionTypes = new ArrayList<>();
        final DBObject query = new BasicDBObject();

        if(filters!=null)
            query.putAll(filters);

        try (DBCursor cursor = connectionTypesCollection.find(query)) {
            while (cursor.hasNext()) {
                DBObject dbObject = cursor.next();
                connectionTypes.add((ConnectionTypeEntity) AppUtils.fromDBObject(dbObject, ConnectionTypeEntity.class));
            }
        }

        return connectionTypes;
    }

    public List<WriteResult> upsertConnectionTypes(@Nonnull final List<ConnectionTypeEntity> connectionTypes) {

        checkNotNull(connectionTypes, "Arguments can not be null");

        final List<WriteResult> results = new ArrayList<>();

        for (ConnectionTypeEntity connectionType : connectionTypes) {

            DBObject query = new BasicDBObject("_id", connectionType.getId());
            DBObject data =  new BasicDBObject("$set", AppUtils.toDBObject(connectionType));

            results.add(connectionTypesCollection.update(query, data, true, false));
        }

        return results;
    }

    public List<WriteResult> deleteConnectionTypes(@Nonnull final List<String> ids) {

        checkNotNull(ids, "Arguments can not be null");
        final List<WriteResult> results = new ArrayList<>();

        for (String id : ids) {

            DBObject query = new BasicDBObject("_id", id);
            results.add(connectionTypesCollection.remove(query));

        }

        return results;
    }

    public void setMongoClient(final MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public void setCollectionName(final String collectionName) {
        this.collectionName = collectionName;
    }

}
