package com.tallink.employees.rest.dao;

import com.mongodb.WriteResult;
import org.bson.Document;

import java.util.List;

public interface ConnectionDao {

    public List<ConnectionEntity> getConnections(Document filters);

    public List<WriteResult> insertConnections(List<ConnectionEntity> connections);

    public List<WriteResult> deleteConnections(List<String> ids);

}
