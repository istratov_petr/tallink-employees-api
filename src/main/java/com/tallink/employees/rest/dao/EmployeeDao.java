package com.tallink.employees.rest.dao;

import com.mongodb.WriteResult;
import org.bson.Document;

import java.util.List;

public interface EmployeeDao {

    public List<EmployeeEntity> getEmployees(Document filters);

    public List<WriteResult> upsertEmployees(List<EmployeeEntity> employees);

    public List<WriteResult> deleteEmployees(List<String> ids);

}
