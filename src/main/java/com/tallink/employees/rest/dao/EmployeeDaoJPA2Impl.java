package com.tallink.employees.rest.dao;

import com.mongodb.*;
import com.tallink.employees.rest.util.AppUtils;
import org.bson.Document;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

class EmployeeDaoJPA2Impl implements EmployeeDao {

    private MongoClient mongoClient;
    private String dbName;
    private String collectionName;
    private DBCollection employeesCollection;

    public void init() throws UnknownHostException {
        DB tallinkDatabase = mongoClient.getDB(dbName);
        employeesCollection = tallinkDatabase.getCollection(collectionName);
    }

    public List<EmployeeEntity> getEmployees(Document filters) {

        final List<EmployeeEntity> employees = new ArrayList<>();
        final DBObject query = new BasicDBObject();

        if(filters!=null)
            query.putAll(filters);

        try (DBCursor cursor = employeesCollection.find(query)) {
             while (cursor.hasNext()) {
                DBObject dbObject = cursor.next();
                employees.add((EmployeeEntity) AppUtils.fromDBObject(dbObject, EmployeeEntity.class));
            }
        }

        return employees;
    }

    public List<WriteResult> upsertEmployees(@Nonnull final List<EmployeeEntity> employees) {

        checkNotNull(employees, "Arguments can not be null");

        final List<WriteResult> results = new ArrayList<>();

        for (EmployeeEntity employee : employees) {

            DBObject query = new BasicDBObject("_id", employee.getId());
            DBObject data =  new BasicDBObject("$set", AppUtils.toDBObject(employee));

            results.add(employeesCollection.update(query, data, true, false));

        }

        return results;
    }

    public List<WriteResult> deleteEmployees(@Nonnull final List<String> ids) {

        checkNotNull(ids, "Arguments can not be null");
        final List<WriteResult> results = new ArrayList<>();

        for (String id : ids) {

            DBObject query = new BasicDBObject("_id", id);
            results.add(employeesCollection.remove(query));

        }

        return results;
    }

    public void setMongoClient(final MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public void setCollectionName(final String collectionName) {
        this.collectionName = collectionName;
    }

}
