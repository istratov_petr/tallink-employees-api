package com.tallink.employees.rest.dao;

import com.tallink.employees.rest.resource.connection.Connection;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;

@Entity
@Table(name="connections")
public class ConnectionEntity implements Serializable {

    private static final long serialVersionUID = -8039686696076337053L;

    @Id
    @GeneratedValue
    @Column(name="_id")
    private String _id;

    @Column(name="connection_types_id")
    private String connection_types_id;

    @Column(name="from")
    private String from;

    @Column(name="to")
    private String to;

    @Column(name="createdAt")
    @DateTimeFormat(iso=DateTimeFormat.ISO.DATE_TIME)
    private Date createdAt;

    @Column(name="owner")
    private String owner;

    @Column(name="userName")
    private String userName;

    @Column(name="privateSwitch")
    private Boolean privateSwitch;

    public ConnectionEntity(){}

    public ConnectionEntity(String _id, String connection_types_id, String from, String to, String owner,
                          String userName, Boolean privateSwitch) {

        this._id = _id;
        this.connection_types_id = connection_types_id;
        this.from = from;
        this.to = to;
        this.owner = owner;
        this.userName = userName;
        this.privateSwitch = privateSwitch;

    }

    public ConnectionEntity(Connection connection){
        try {
            BeanUtils.copyProperties(this, connection);
        } catch (IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public String getId() {
        return _id;
    }

    public void setId(String _id) {
        this._id = _id;
    }

    public String getConnection_types_id() {
        return connection_types_id;
    }

    public void setConnection_types_id(String connection_types_id) { this.connection_types_id = connection_types_id; }

    public String getTo() {
        return to;
    }

    public void setTo(String to) { this.to = to; }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) { this.from = from; }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) { this.createdAt = createdAt; }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) { this.owner = owner; }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) { this.userName = userName; }

    public Boolean getPrivateSwitch() {
        return privateSwitch;
    }

    public void setPrivateSwitch(Boolean privateSwitch) { this.privateSwitch = privateSwitch; }

}
