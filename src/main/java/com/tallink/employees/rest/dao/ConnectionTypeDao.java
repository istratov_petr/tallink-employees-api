package com.tallink.employees.rest.dao;

import com.mongodb.WriteResult;
import org.bson.Document;

import java.util.List;

public interface ConnectionTypeDao {

    public List<ConnectionTypeEntity> getConnectionTypes(Document filters);

    public List<WriteResult> upsertConnectionTypes(List<ConnectionTypeEntity> connectionTypes);

    public List<WriteResult> deleteConnectionTypes(List<String> ids);

}
