package com.tallink.employees.rest.dao;

import com.mongodb.*;
import org.bson.Document;
import com.tallink.employees.rest.util.AppUtils;

import javax.annotation.Nonnull;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

public class ConnectionDaoJPA2Impl implements ConnectionDao {

    private MongoClient mongoClient;
    private String dbName;
    private String collectionName;
    private DBCollection connectionsCollection;

    public void init() throws UnknownHostException {
        DB tallinkDatabase = mongoClient.getDB(dbName);
        connectionsCollection = tallinkDatabase.getCollection(collectionName);
    }

    public List<ConnectionEntity> getConnections(Document filters) {

        final List<ConnectionEntity> connections = new ArrayList<>();
        final DBObject query = new BasicDBObject();

        if(filters!=null)
            query.putAll(filters);

        try (DBCursor cursor = connectionsCollection.find(query)) {
            while (cursor.hasNext()) {
                DBObject dbObject = cursor.next();
                connections.add((ConnectionEntity) AppUtils.fromDBObject(dbObject, ConnectionEntity.class));
            }
        }

        return connections;
    }

    public List<WriteResult> insertConnections(@Nonnull final List<ConnectionEntity> connections) {

        checkNotNull(connections, "Arguments can not be null");
        final List<WriteResult> results = new ArrayList<>();

        for (ConnectionEntity connection : connections) {

            DBObject data =  AppUtils.toDBObject(connection);
            results.add(connectionsCollection.insert(data));

        }

        return results;
    }

    public List<WriteResult> deleteConnections(@Nonnull final List<String> ids) {

        checkNotNull(ids, "Arguments can not be null");
        final List<WriteResult> results = new ArrayList<>();

        for (String id : ids) {

            DBObject query = new BasicDBObject("_id", id);
            results.add(connectionsCollection.remove(query));

        }

        return results;
    }

    public void setMongoClient(final MongoClient mongoClient) {
        this.mongoClient = mongoClient;
    }

    public void setDbName(final String dbName) {
        this.dbName = dbName;
    }

    public void setCollectionName(final String collectionName) {
        this.collectionName = collectionName;
    }

}
